1. program < plik
2. program > plik (nadpisanie)(tworzy plik jak go nie ma)
3. program >> plik (dopisanie)

zad.1
ls -l > plik (stworzenie plik z informacją z komendy)

zad.2
cat > plik (nadpisywanie pliku kolejnymi linijkami)
cat >> plik (dopisywanie do pliku)
ctrl+D (EOF)

cat - wypisywanie w konsoli

zad.3 
echo -n abcdefghijklmn > plik (wpisywanie do pliku)
exec 3<> plik (otwarcie pliku do zapisu i odczytu przy użyciu deskryptora 3)
read -n 5 <&3 (przesunięcie miejsca bieżącego dostępu do zawartości otwartego pliku o 5 pozycji do przodu)
echo -n ABC >&3 (dopisanie do pliku)
exec 3>&- (zamknięcie deskryptora 3)

zad.4
program_1 | program_2 (uruchamia oba programy, tak jakby AND)
można spamic np: 
program | program | program | program (wykonują się po kolei)

zad.5
gcc program.c > plik
(możliwość zapisania błędów w plikach)

zad.6
ls -l katalog | more 
cat program | more 
more < program (pokazuje linijkami w terminalu)
cat program | less (slash do szukania w pliku, Q zakończ)

zad.7
```
cat plik | sort
sort < plik
sort plik
```
^ pokazanie pliku posortowanego(bez zmian w pliku)
sort < plik > plik_sorted (zapisanie nowego pliku z posortowaną zawartością)
sort > plik (wpisywanie do pliku tresci + sortowanie)

zad.8
a) head -n 5 plik
b) tail -n 3 plik

zad.9
tee plik > plik1 (zapisuje do obu plików to samo)

zad.10
stat file (wyswietla wielkosc i inne informacje)
ln file1 file2 (dowiazania)
