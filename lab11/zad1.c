#include <stdio.h>
#include <sys/types.h> 
#include <unistd.h> 

int main(){ 

	printf("To ja główny proces \n");
 	  
	int id = fork(); // po tej instrukcji wspołbieżnie zostaje uruchomiony proces z kodem znajdującym się poniżej

	printf("To ja proces potomny lub rodzicielski \n"); 

	return 0; 
}